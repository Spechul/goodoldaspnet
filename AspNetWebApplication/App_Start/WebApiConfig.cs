﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace AspNetWebApplication
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Конфигурация и службы веб-API

            // Маршруты веб-API
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { controller="values", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute("notfound",
                "{*any}",
                new { controller = "Error", action="NotFoundApi" });
            /*config.Routes.MapHttpRoute("404-catch-all",
                "api/{*catchall}",
                new { controller = "Error", action = "NotFoundApi"});*/
        }
    }
}
