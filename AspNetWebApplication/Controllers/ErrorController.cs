﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace AspNetWebApplication.Controllers
{
    //[RoutePrefix("api/error")]
    public class ErrorController : ApiController
    {
        [HttpGet]
        [HttpPost]
        [HttpPatch]
        [HttpPut]
        [HttpDelete]
        [HttpOptions]
        [HttpHead]
        //[Route("notfoundapi")]
        public IHttpActionResult NotFoundApi()
        {
            return Json(new NotFoundResult(new HttpRequestMessage(Request.Method, Request.RequestUri)));
        }
    }
}