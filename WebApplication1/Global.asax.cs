using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using WebApplication1.Config;
using WebApplication1.Selectors;

namespace WebApplication1
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            var configuration = GlobalConfiguration.Configuration;
            configuration.Services.Replace(typeof(IHttpControllerSelector), new HttpNotFoundAwareDefaultHttpControllerSelector(configuration));
            configuration.Services.Replace(typeof(IHttpActionSelector), new HttpNotFoundAwareControllerActionSelector());
        }
    }
}
