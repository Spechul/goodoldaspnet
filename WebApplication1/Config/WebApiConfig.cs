﻿using System.Web.Http;

namespace WebApplication1.Config
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Конфигурация и службы веб-API

            // Маршруты веб-API
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}"//,
                //defaults: new { controller = "Values", action = "List", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute("notfound",
                "api/{*any}",
                new { controller = "Error", action = "ApiNotFound" });
            /*config.Routes.MapHttpRoute("404-catch-all",
                "api/{*catchall}",
                new { controller = "Error", action = "NotFoundApi"});*/
        }
    }
}