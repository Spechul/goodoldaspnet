﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace WebApplication1.Controllers
{
    public class ErrorController : ApiController
    {
        [HttpGet, HttpPost, HttpPut, HttpDelete, HttpHead, HttpOptions, AcceptVerbs("PATCH")]
        public IHttpActionResult ApiNotFound()
        {
            return Json(new NotFoundResult(new HttpRequestMessage(Request.Method, Request.RequestUri)));
        }
    }
}